Tasks done by each member:

Митинев Дмитрий:
1. Создал репозиторий, зарегистрировался и получил токен.
2. Создал класс Request и методы в нем.
3. Создал класс Card и метод createCardTherapist(), передал объект дата в класс Card, вызвал метод класса Request на создание карточки.

Павло Климчук:
1. Создал класс Personalisation, метод logIn(), loginToThePage(), allCardsLength() в класcе Request.
2. Создал объект и отправил на сервер. Класс Visit, VisitDentist, VisitCardiologist, VisitTherapist.
3. Сделал фильтрацию. Класс Filter и методы в нем.
4. Написал метод по удалению надписи ( No items have been added ) и карточек с базы и с экрана. Класс DeleteCards и методы delCard(), delTextNoItem().
5. Написал функционал по изменению кнопок войти и создать визит.
6. Исправилял некоторые баги по проекту, например баг с кнопкой showMore, сhangeCard.

Гудзенко Анна:
1. Создала структуру проекта, подключила bootstrap, axios. Создала дизайн страницы, формы логина, модального окна, карточек.
2. Создала модальное окно (форму) ввода данных, форму логина. Класс Modal, ModalLogin, Input, Select, Textarea, Label, с их методами.
3. Написала функционал по изменения поля ввода доктор, функционал по закрытию модального окна. Класс FormToAddFields, с его методами.
4. Сделала редактирования, класс EditInfo, реквест на PUТ и функционал на редактирования в классах VisitDentist, VisitCardiologist, VisitTherapist.
5. Создала функционал и визуальную часть метода showMore() класса DeleteCards.
6. Методы: createCardDentis(), createCardCardiologist() класса Card.
7. Сделала проверку на валидацию пустого значения в классах VisitDentist, VisitCardiologist, VisitTherapist. Метод removeCard() в классе Visit.
8. Исправиляла некоторые баги по проекту.
